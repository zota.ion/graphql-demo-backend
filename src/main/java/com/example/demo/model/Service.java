package com.example.demo.model;

import com.example.demo.common.ServiceType;
import com.example.demo.common.Unit;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table("service")
public class Service extends BaseModel {

    @Column
    private String name;
    @Column("service_type")
    private ServiceType serviceType;
    @Column
    private Unit unit;
    @Column
    private Integer amount;

    public Service(){}

    public Service(String name, ServiceType serviceType, Unit unit, Integer amount) {
        this.name = name;
        this.serviceType = serviceType;
        this.unit = unit;
        this.amount = amount;
        this.setCreateDate(LocalDateTime.now());
        this.setId(UUID.randomUUID());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Service{" +
                "name='" + name + '\'' +
                ", serviceType=" + serviceType +
                ", unit=" + unit +
                ", amount=" + amount +
                '}';
    }
}
