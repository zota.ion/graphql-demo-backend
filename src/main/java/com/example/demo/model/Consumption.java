package com.example.demo.model;

import com.example.demo.common.Unit;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table("consumption")
public class Consumption extends BaseModel{

    @Column("subscription_id")
    private UUID subscriptionId;
    @Column("service_id")
    private UUID serviceId;
    @Column("user_id")
    private UUID userId;
    @Column
    private Unit unit;
    @Column
    private Integer amount;
    @Column("country_code")
    private String countryCode;

    public Consumption(){}

    public Consumption(UUID subscriptionId, UUID serviceId, UUID userId, Unit unit, Integer amount, String countryCode) {
        this.subscriptionId = subscriptionId;
        this.serviceId = serviceId;
        this.userId = userId;
        this.unit = unit;
        this.amount = amount;
        this.countryCode = countryCode;
        this.setCreateDate(LocalDateTime.now());
        this.setId(UUID.randomUUID());
    }

    public UUID getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(UUID subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public UUID getServiceId() {
        return serviceId;
    }

    public void setServiceId(UUID serviceId) {
        this.serviceId = serviceId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "Consumption{" +
                "subscriptionID=" + subscriptionId +
                ", serviceID=" + serviceId +
                ", userId=" + userId +
                ", unit=" + unit +
                ", amount=" + amount +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}
