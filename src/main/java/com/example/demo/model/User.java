package com.example.demo.model;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table("user")
public class User extends BaseModel{

    @Column("first_name")
    private String firstName;
    @Column("last_name")
    private String lastName;
    @Column("msisdn")
    private Long msisdn;
    @Column("country_code")
    private String countryCode;

    public User(){}

    public User(String firstName, String lastName, Long msisdn, String countryCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.msisdn = msisdn;
        this.countryCode = countryCode;
        this.setCreateDate(LocalDateTime.now());
        this.setId(UUID.randomUUID());
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", msisdn=" + msisdn +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}
