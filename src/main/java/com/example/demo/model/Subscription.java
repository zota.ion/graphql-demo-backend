package com.example.demo.model;

import com.example.demo.common.Unit;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table("subscription")
public class Subscription extends BaseModel {

    @Column("user_id")
    private UUID userId;
    @Column("service_id")
    private UUID serviceId;
    @Column("unit")
    private Unit unit;
    @Column("amount")
    private Integer amount;


    public Subscription(){}

    public Subscription(UUID userId, UUID serviceId, Unit unit, Integer amount){
        this.userId = userId;
        this.serviceId = serviceId;
        this.unit = unit;
        this.amount = amount;
        this.setCreateDate(LocalDateTime.now());
        this.setId(UUID.randomUUID());
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getServiceId() {
        return serviceId;
    }

    public void setServiceId(UUID serviceId) {
        this.serviceId = serviceId;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "userId=" + userId +
                ", serviceId=" + serviceId +
                ", unit=" + unit +
                ", amount=" + amount +
                '}';
    }

}
