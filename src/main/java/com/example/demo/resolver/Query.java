package com.example.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.demo.model.Consumption;
import com.example.demo.model.Service;
import com.example.demo.model.Subscription;
import com.example.demo.model.User;
import com.example.demo.repository.ConsumptionRepository;
import com.example.demo.repository.ServiceRepository;
import com.example.demo.repository.SubscriptionRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Component;

import java.util.List;
public class Query implements GraphQLQueryResolver {


    private UserRepository userRepository;
    private SubscriptionRepository subscriptionRepository;
    private ServiceRepository serviceRepository;
    private ConsumptionRepository consumptionRepository;

    public Query(UserRepository userRepository, SubscriptionRepository subscriptionRepository, ServiceRepository serviceRepository, ConsumptionRepository consumptionRepository) {
        this.userRepository = userRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.serviceRepository = serviceRepository;
        this.consumptionRepository = consumptionRepository;
    }

    public Slice<User> findAllUsers(Integer size){
      return userRepository.findAll(CassandraPageRequest.first(size));
    }
    public User findUserByMsisdn(Long msisdn){return userRepository.findUserByMsisdn(msisdn);}

    public List<User> findByMsisdnLessThan(Long i){
        return userRepository.findByMsisdnLessThan(i);
    }

    public List<User> findByFirstNameLike(String name){
        return userRepository.findByFirstNameLike(name);
    }

    public List<Subscription> findAllSubscriptions(){return  subscriptionRepository.findAll();}

    public List<Service> findAllServices(){ return serviceRepository.findAll();}

    public List<Consumption> findAllConsumptions(){return  consumptionRepository.findAll();}
}
