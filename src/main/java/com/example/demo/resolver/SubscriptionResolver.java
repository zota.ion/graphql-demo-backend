package com.example.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.example.demo.model.Service;
import com.example.demo.model.Subscription;
import com.example.demo.model.User;
import com.example.demo.repository.ServiceRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class SubscriptionResolver implements GraphQLResolver<Subscription> {


    UserRepository userRepository;
    ServiceRepository serviceRepository;

    public SubscriptionResolver(UserRepository userRepository, ServiceRepository serviceRepository) {
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
    }

    public User getUser(Subscription subscription){

        return  userRepository.findById(subscription.getUserId()).get();

    }

    public Service getService(Subscription subscription){

        return serviceRepository.findById(subscription.getServiceId()).get();
    }


}
