package com.example.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.example.demo.model.Consumption;
import com.example.demo.model.Service;
import com.example.demo.model.Subscription;
import com.example.demo.model.User;
import com.example.demo.repository.ServiceRepository;
import com.example.demo.repository.SubscriptionRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class ConsumptionResolver implements GraphQLResolver<Consumption> {


    private UserRepository userRepository;
    private ServiceRepository serviceRepository;
    private SubscriptionRepository subscriptionRepository;

    public ConsumptionResolver(UserRepository userRepository, ServiceRepository serviceRepository, SubscriptionRepository subscriptionRepository) {
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
        this.subscriptionRepository = subscriptionRepository;
    }

    public User getUser(Consumption consumption) {

        return userRepository.findById(consumption.getUserId()).get();

    }

    public Service getService(Consumption consumption) {

        return serviceRepository.findById(consumption.getServiceId()).get();
    }

    public Subscription getUserSubscription(Consumption consumption) {

        return subscriptionRepository.findById(consumption.getSubscriptionId()).get();
    }

}
