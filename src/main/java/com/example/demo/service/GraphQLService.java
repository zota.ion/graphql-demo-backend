package com.example.demo.service;

import com.coxautodev.graphql.tools.SchemaParser;
import com.example.demo.repository.ConsumptionRepository;
import com.example.demo.repository.ServiceRepository;
import com.example.demo.repository.SubscriptionRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.resolver.*;
import graphql.schema.GraphQLSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


import graphql.GraphQL;

import java.io.IOException;


@Service
public class GraphQLService {

    private GraphQL graphQL;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    SubscriptionRepository subscriptionRepository;
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    ConsumptionRepository consumptionRepository;

    @PostConstruct
    private void loadSchema() throws IOException {

        graphQL = GraphQL.newGraphQL(buildSchema()).build();
    }

    private GraphQLSchema buildSchema() {
        return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(
                        new Query(userRepository, subscriptionRepository, serviceRepository, consumptionRepository)
                        , new Mutation(userRepository, subscriptionRepository, serviceRepository, consumptionRepository)
                        , new SubscriptionResolver(userRepository, serviceRepository)
                        , new ConsumptionResolver(userRepository, serviceRepository, subscriptionRepository)
                )
                .build()
                .makeExecutableSchema();
    }

    public GraphQL getGraphQL() {
        return graphQL;
    }
}
