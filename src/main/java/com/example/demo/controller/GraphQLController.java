package com.example.demo.controller;


import com.example.demo.service.GraphQLService;
import com.example.demo.utill.QueryParameters;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/graphql")
@RestController
public class GraphQLController {

    @Autowired
    GraphQLService graphQLService;

    @PostMapping
    public ResponseEntity<Object> postRequest(@RequestBody String query) {

        return new ResponseEntity<>(processRequest(query), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Object> getRequest(@RequestBody String query) {

        return new ResponseEntity<>(processRequest(query), HttpStatus.OK);
    }

    private ExecutionResult processRequest(String query) {

        QueryParameters parameters =  QueryParameters.from(query);

        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
                .query(parameters.getQuery())
                .variables(parameters.getVariables())
                .operationName(parameters.getOperationName())
                .build();

        ExecutionResult result = graphQLService.getGraphQL().execute(executionInput);
        return result;
    }
}
