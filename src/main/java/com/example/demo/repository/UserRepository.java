package com.example.demo.repository;



import com.example.demo.model.User;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public interface UserRepository extends CassandraRepository<User, UUID> {

    @Query(allowFiltering = true)
    User findUserByMsisdn(Long msisdn);

    Slice<User> findAll(Pageable pageRequest);

    @Query(allowFiltering = true)
    List<User> findByMsisdnLessThan(Long i);

    @Query(allowFiltering = true)
    List<User> findByFirstNameLike(String name);

}